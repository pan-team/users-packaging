# Software for Users

This is where Users from photon/neutron facilities can add their wishes and efforts. 
You are very welcome to edit this README and add your comments and/or initiatives.

The software should better have its source code fully available, and maintained. 
Please use the *Category* column to indicate roughtly what the software is used for. 
The *Status* may indicate if the software is active, if there are any packaging attempts, etc.

| Package | Category | Link | Status    | Remarks      |
|---------|----------|------|-----------|--------------|
